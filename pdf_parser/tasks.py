import os
import pdfx
from pdfx.downloader import get_status_code
from comeet_pdf_crawler.celery_app import app
from pdf_parser.models import Document, URL
from django.conf import settings


def delete_file(path):
    try:
        os.remove(path)
    except OSError:
        pass


@app.task()
def check_link(url_id):
    url = URL.objects.get(id=url_id)
    status_code = str(get_status_code(url.url))
    if status_code == '200':
        url.is_alive = True
    else:
        url.is_alive = False
    url.save()


@app.task()
def parse_pdf(document_name):
    full_path = os.path.join(settings.MEDIA_ROOT, document_name)
    pdf = pdfx.PDFx(full_path)
    references_list = pdf.get_references()
    refs = [ref for ref in references_list if ref.reftype in ["url", "pdf"]]
    document = Document(name=document_name, number_of_urls=len(refs))
    document.save()
    for item in refs:
        url = URL.objects.filter(url=item.ref).first()
        if url is None:
            url = URL(url=item.ref)
            url.save()
        url.document_set.add(document)
        url.save()
        check_link.delay(url_id=url.id)
    delete_file(full_path)

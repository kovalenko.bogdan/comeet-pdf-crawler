# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.http import HttpResponseRedirect
from django.urls import reverse
from rest_framework.views import APIView
from rest_framework.parsers import FormParser
from rest_framework.response import Response
from pdf_parser.models import Document, URL
from pdf_parser.serializers import DocumentSerializer, URLSerializer, URLCountedSerializer
from rest_framework import status
from django.conf import settings
from django.db.models import Count
from django.shortcuts import render


class DocumentView(APIView):
    """
    API endpoint that allows view Documents.
    """
    def get(self, request):
        queryset = Document.objects.all().order_by('id')
        serializer = DocumentSerializer(queryset, many=True)
        return Response(serializer.data)


class DocumentDetail(APIView):
    """
    API endpoint that allows view Documents detail.
    """
    def get(self, request, document_id):
        queryset = URL.objects.filter(document__id=document_id).order_by('id')
        if queryset is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = URLSerializer(queryset, many=True)
        return Response(serializer.data)


class URLView(APIView):
    """
    API endpoint that allows view set of all parserd URL's.
    """
    def get(self, request):
        queryset = URL.objects.annotate(count=Count("document")).order_by('id')
        serializer = URLCountedSerializer(queryset, many=True)
        return Response(serializer.data)


class FileUploadView(APIView):
    """
    API endpoint that allows to upload a document (submit for parsing).
    """
    parser_class = (FormParser,)

    def post(self, request):
        file = request.data.get('file')
        if not file or file.content_type != 'application/pdf':
            return Response(status=status.HTTP_400_BAD_REQUEST)
        filename = file.name
        full_path = os.path.join(settings.MEDIA_ROOT, filename)
        with open(full_path, 'w') as file_handler:
            for chunk in file.chunks():
                file_handler.write(chunk)
        from pdf_parser.tasks import parse_pdf
        parse_pdf.delay(filename)
        return HttpResponseRedirect(reverse('index'))


def index(request):
    """
    Home page
    """
    return render(request, 'pdf_parser/index.html')

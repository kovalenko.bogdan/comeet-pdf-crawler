# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class URL(models.Model):
    # https://docs.djangoproject.com/en/1.11/topics/db/models/#automatic-primary-key-fields
    url = models.URLField(max_length=2048)
    # None will indicate that we didn't performed check
    is_alive = models.NullBooleanField(null=True, blank=True)

    def __unicode__(self):
        return self.url

class Document(models.Model):
    # https://docs.djangoproject.com/en/1.11/topics/db/models/#automatic-primary-key-fields
    name = models.CharField(max_length=255)
    number_of_urls = models.PositiveSmallIntegerField(default=0)
    urls = models.ManyToManyField(URL)

    def __unicode__(self):
        return self.name

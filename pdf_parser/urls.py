from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^upload/$', views.FileUploadView.as_view(), name='upload'),
    url(r'^urls/$', views.URLView.as_view(), name='all-urls'),
    url(r'^documents/$', views.DocumentView.as_view(), name='document'),
    url(r'^documents/(?P<document_id>[0-9]+)/urls/$', views.DocumentDetail.as_view(), name='document-urls'),
]

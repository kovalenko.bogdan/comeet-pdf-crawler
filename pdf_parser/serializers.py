from pdf_parser.models import Document, URL
from rest_framework import serializers


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ('id', 'name', 'number_of_urls')


class URLSerializer(serializers.ModelSerializer):
    class Meta:
        model = URL
        fields = '__all__'


class URLCountedSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField(read_only=True)

    class Meta:
        model = URL
        fields = '__all__'

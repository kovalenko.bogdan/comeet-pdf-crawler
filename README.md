# Commet home assigment

## Django PDF parser

Demo - http://wiki.kova.ml:8000/

Stack:
 - `python 2.7`
 - `docker` - app containerisation https://docs.docker.com/install/
 - `django_rest_framework` - REST endpoints
 - `pdfx` - PDF parsing and URL checking
 - `celery` - background tasks
 - `redis` - messege brocker for the `celery`
 - `postresql` - relational database

I use `pdfx` library because it basically can do only two things:
  - get links from PDF
  - check if the links is alive

(It pretty old - last commit was at 2016. But it fully covers my needs)

https://github.com/metachris/pdfx

https://pypi.org/project/pdfx/

I used `rest_framework` to setup endpoints and `celery` tasks to parse and save data.


All application is wrapped in the docker 
so you can start application just with one command `make up` or `docker-compose up`.
It will automatically start all required services including database.
Also, it will apply migration as well.

To run this application you need a free `8000` port on your machine.

I used to `docker` in order to achieve environment isolation especially with `python 2.7`.
`rest_framewor` do not supports `python 2.7` in the new releases, so I used older versions.

There is validation for `pdf` file type on file upload.

Plese, do not judge me for all-in-one html file (was created just to demonstrate backend).
I also skipped right setting of env variables, so this app far from https://12factor.net/.
But it shouldn't influence app until it's in the docker (but I would definitely fix this).


### Document upload
`http://0.0.0.0:8000/api/upload/`

Here document that you can use to test parser

https://weakdh.org/imperfect-forward-secrecy.pdf

**Method POST**

Form data with data:
`file` - binary data (accepts just `application/pdf`)

### Get all Documents
`http://0.0.0.0:8000/api/documents/`

**Method GET**

`number_of_urls` - indicates total amount of url in the document
```json
[
    {
        "id": 1,
        "name": "first.pdf",
        "number_of_urls": 12
    },
    {
        "id": 2,
        "name": "last.pdf",
        "number_of_urls": 33
    }
]
```

### Get Urls for specific document 
`http://0.0.0.0:8000/api/documents/<id>/urls/`

**Method GET**

`is_alive` - indicates if link responds with `200` status

```json
[
    {
        "id": 1,
        "url": "https://py.checkio.org/user/test/",
        "is_alive": true
    },
    {
        "id": 2,
        "url": "spalah.com",
        "is_alive": false
    }
]
```

### Get all Urls with number of occurrences 
`http://0.0.0.0:8000/api/urls/`

**Method GET**

`count` - indicates number of occurrences in the documents

```json
[
    {
        "id": 1,
        "count": 13,
        "url": "https://py.checkio.org/user/test/",
        "is_alive": true
    },
    {
        "id": 2,
        "count": 13,
        "url": "spalah.com",
        "is_alive": false
    }
]
```
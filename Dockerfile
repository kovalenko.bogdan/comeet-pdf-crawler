FROM python:2.7

ENV PYTHONUNBUFFERED 1

# Install requirements in a separate step for caching
WORKDIR /code
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Install application code
COPY . .
